module.exports = {
            registrationTokenId : String,
            hardwareId : String,
            lastModified : String,
            channels : [String],
            courses : [String]
        }